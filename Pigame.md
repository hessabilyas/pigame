# Les Trois Petits Cochons
## Le Jeu

Dans ce jeu, nous jouons le loup. Son but est de manger les trois petits cochons mais pour cela il devra surmonter quelques obstacles.

## Comment jouer ?

Pour se déplacer, il suffit d'utiliser es flèches directionnelles et de suivre les instructions afin d'accomplir son objectif.

## Difficultées rencontrées et Solutions

### 1 - Le déplacement

Faire en sorte que quand on appuie sur une des fléches directionnelles le loup se déplace dans une direction.
Pour palier à cette difficulté, j'ai eu recours à "pygame.KEYDOWN"

### 2 - La musique

Les fichiers "mp3" ne fonctionnaient pas j'ai essayé de trouver pourquoi sans succès, j'ai donc eu l'idée de les passés en ".ogg"
