# Importations
import pygame
from easygui import*
from time import sleep

pygame.init()

# Surface
res = (1000, 600)
screen = pygame.display.set_mode(res)

# Setup image
loup = pygame.image.load("Loup.png")
fond_b = pygame.image.load("fond_vert.png")
fond_c = pygame.image.load("fond_combat.png")
cochon_paille = pygame.image.load("Cochon de paille.png")
maison_paille = pygame.image.load("maison de paille.png")
cochon_bois = pygame.image.load("cochon de bois.png")
maison_bois = pygame.image.load("maison de bois.png")
cochon_brique = pygame.image.load("cochon de brique.png")
maison_brique = pygame.image.load("maison de brique.png")
texte_maison = pygame.image.load("texte vers maison.png")
ecran_victoire = pygame.image.load("Ecran de victoire.png")
shop = pygame.image.load("Shop.png")
coffre = pygame.image.load("Coffre.png")
loading = pygame.image.load("loading.png")
screen.blit(loading, (0, 0))
pygame.display.flip()

S_Feu = pygame.mixer.Sound("Feu.ogg")
S_BDF = pygame.mixer.Sound("Bruit de fond.ogg")
S_soufflement = pygame.mixer.Sound("Soufflement.ogg")
S_soufflement.set_volume(0.3)

# Initialisation de toutes les variables
"""Il y a 4 modes, les changer permettra de changer de fond et d'avancer dans l'histoire."""
mode = "1"
"""XL et YL correspondent aux coordonnées du loup tandis que XC et YC à ceux du cochon"""
XL = 500
YL = 300
XC = 800
YC = 200
HP_mob = 0
HP_S = 0
regen_mob = 0
attaque_L = 50
message = True
message2 = True
disp = True
affichage = True
choixM1 = ""
choixM2 = ""
choixM3 = ""
"""PO = Pièce d'Or"""
PO = 0
magasin = ["Allumettes : 100PO", "Griffes métalique : 1000PO"]
allumette = False
griffes_M = False

# Fonction
def UP(YL: int) -> int:
    """
    take YL
    change YL
    return new YL
    """
    if YL <= 0:
        pass
    else:
        YL = YL - 100
    return(YL)
def DOWN(YL: int) -> int:
    """
    take YL
    change YL
    return new YL
    """
    if YL >= 500:
        pass
    else:
        YL = YL + 100
    return(YL)
def LEFT(XL: int) -> int:
    """
    take XL
    change XL
    return new XL
    """
    if XL <= 0:
        pass
    else:
        XL = XL - 100
    return(XL)
def RIGHT(XL: int) -> int:
    """
    take XL
    change XL
    return new XL
    """
    if XL >= 900:
        pass
    else:
        XL = XL + 100
    return(XL)

# Bruit de fond
S_BDF.play(100)
# Boucle
launched = True
while launched:
    for event in pygame.event.get():
        """Si on clique sur la croix le jeu se ferme"""
        if event.type == pygame.QUIT:
            launched = False
        """
        Si une flèche dirrectionnelle est pressée la fonction correspondant à la direction s'activera et le loup se déplacera
        """
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP:
                YL = UP(YL)
            elif event.key == pygame.K_DOWN:
                YL = DOWN(YL)
            elif event.key == pygame.K_LEFT:
                XL = LEFT(XL)
            elif event.key == pygame.K_RIGHT:
                XL = RIGHT(XL)

    # Act 1
    if mode == "1":
        screen.blit(fond_b, (0, 0))
        if choixM1 == "Souffler":
            """HP_S permet de différencier les trois cochons"""
            HP_S = 100
            HP_mob = 100
            mode = "C"
            S_soufflement.play(0, 2000, 1500)
            sleep(1.5)
        else:
            screen.blit(maison_paille, (800, 0))
        if XL == XC and YL == YC:
            """Permet de faire disparaître le cochon. """
            disp = False
            screen.blit(texte_maison, (700, 300))
        else:
            """Faire fonctionner la boutique."""
            if disp:
                screen.blit(cochon_paille, (XC, YC))
        if XL == 200 and YL == 400 and message2 :
            choix_S = choicebox("Que veux tu acheter ? ", "Magasin", magasin)
            message2 = False
            if choix_S == "Allumettes : 100PO":
                if PO >= 100:
                    allumette = True
                    PO = PO - 100
                    print("Allumettes achetées")
                else:
                    print("Vous n'avez pas assez d'argent")
            elif choix_S == "Griffes métalique : 1000PO":
                if PO >= 1000:
                    griffes_M = True
                    PO = PO - 1000
                    print("Griffes métaliques achetées")
                    attaque_L = attaque_L + 100
                else:
                    print("Vous n'avez pas assez d'argent")
        if disp == False and XL == 800 and YL == 100 and message == True:
            choixM1 = buttonbox("Rrrr ! Tu vas voir ce que tu vas voir, Ta maison de paille je vais la faire s'envoler!", "test", ["Souffler", "Cancel"])
            message = False
        else:
            pass
        if not (XL == 800 and YL == 100) and not (XL == 200 and YL == 400):
            message = True
            message2 = True
        screen.blit(shop, (200, 400))
        screen.blit(loup, [XL, YL])

    # ACT 2
    elif mode == "2":
        screen.blit(fond_b, (0, 0))
        if choixM2 == "Brûler":
            HP_S = 150
            HP_mob = 150
            mode = "C"
            S_Feu.play(0, 2500)
            sleep(1.5)
        else:
            screen.blit(maison_bois, (800, 0))
        if XL == XC and YL == YC:
            disp = False
            screen.blit(texte_maison, (700, 300))
        else:
            if disp:
                screen.blit(cochon_bois, (XC, YC))
        if XL == 200 and YL == 400 and message2:
            choix_S = choicebox("Que veux tu acheter ? ", "Magasin", magasin)
            message2 = False
            if choix_S == "Allumettes : 100PO":
                if PO >= 100:
                    allumette = True
                    PO = PO - 100
                    print("Allumettes achetées")
                else:
                    print("Vous n'avez pas assez d'argent")
            elif choix_S == "Griffes métalique : 1000PO":
                if PO >= 1000:
                    griffes_M = True
                    PO = PO - 1000
                    print("Griffes métaliques achetées")
                    attaque_L = attaque_L + 100
                else:
                    print("Vous n'avez pas assez d'argent")
        if disp == False and XL == 800 and YL == 100 and message == True:
            if allumette:
                choixM2 = buttonbox("Rrrr ! Tu vas voir ce que tu vas voir, Ta maison de bois je vais la faire s'envoler!", "test", ["Souffler", "Brûler" , "Cancel"])
                if choixM2 == "Souffler":
                    S_soufflement.play(0, 2000, 1500)
                    sleep(1.5)
                    msgbox("La maison est trop résistante")
                message = False
            else:
                choixM2 = buttonbox("Rrrr ! Tu vas voir ce que tu vas voir, Ta maison de bois je vais la faire s'envoler!", "test",["Souffler", "Cancel"])
                if choixM2 == "Souffler":
                    msgbox("La maison est trop résistante")
                message = False
        else:
            pass
        if not (XL == 800 and YL == 100) and not (XL == 200 and YL == 400):
            message = True
            message2 = True
        screen.blit(shop, (200, 400))
        screen.blit(loup, [XL, YL])

    # ACT 3
    elif mode == "3":
        screen.blit(fond_b, (0, 0))
        if choixM3 == "Brûler":
            HP_S = 300
            HP_mob = 300
            regen_mob = 50
            mode = "C"
            S_Feu.play(0, 2500)
            msgbox("Vous avez brûlez la porte")

        else:
            screen.blit(maison_brique, (800, 0))
        if XL == XC and YL == YC:
            disp = False
            screen.blit(texte_maison, (700, 300))
        else:
            if disp:
                screen.blit(cochon_brique, (XC, YC))
        if XL == 200 and YL == 400 and message2:
            choix_S = choicebox("Que veux tu acheter ? ", "Magasin", magasin)
            message2 = False
            if choix_S == "Allumettes : 100PO":
                if PO >= 100:
                    allumette = True
                    PO = PO - 100
                    print("Allumettes achetées")
                else:
                    print("Vous n'avez pas assez d'argent")
            elif choix_S == "Griffes métalique : 1000PO":
                if PO >= 1000:
                    griffes_M = True
                    PO = PO - 1000
                    print("Griffes métaliques achetées")
                    attaque_L = attaque_L + 100
                else:
                    print("Vous n'avez pas assez d'argent")
        if disp == False and XL == 800 and YL == 100 and message == True:
            choixM3 = buttonbox("Rrrr ! Tu vas voir ce que tu vas voir, Ta maison de brique je vais la faire s'envoler!", "test", ["Souffler", "Brûler" , "Cancel"])
            if choixM3 == "Souffler":
                msgbox("La maison est trop résistante")
            elif choixM3 == "Brûler":
                S_Feu.play(0, 2500)
                msgbox("Vous avez brûlez la porte")
                HP_S = 300
                HP_mob = 300
                regen_mob = 50
                mode = "C"

            message = False
        else:
            pass
        if not (XL == 800 and YL == 100) and not (XL == 200 and YL == 400):
            message = True
            message2 = True
        screen.blit(shop, (200, 400))
        screen.blit(loup, [XL, YL])

    # Combat
    elif mode == "C":
        # Cochon de paille
        if HP_S == 100:
            if affichage:
                screen.blit(fond_c, (0, 0))
                screen.blit(loup, [100, 200])
                screen.blit(cochon_paille, (XC, YC))
                affichage = False
            elif HP_mob > 0:
                cont = ccbox("Dévorer ce cochon ou le laisser grossir ?", "Continuer ?")
                if cont == 1:
                    attaque_C = buttonbox("Que veux tu faire ?", "Attaque", ["Griffe", "Morsure"])
                    HP_mob = HP_mob - attaque_L
                    print(HP_mob)
                    affichage = True
                else:
                    mode = "1"
                    choixM1 = False
                    affichage = True
                    XL = 0
                    YL = 100
            else:
                mode = "2"
                PO = PO + 100
                choixM1 = False
                affichage = True
                disp = True
                XL = 0
                YL = 100

        # Cochon de bois
        elif HP_S == 150:
            if affichage:
                screen.blit(fond_c, (0, 0))
                screen.blit(loup, [100, 200])
                screen.blit(cochon_bois, (XC, YC))
                affichage = False
            elif HP_mob > 0:
                cont = ccbox("Dévorer ce cochon ou le laisser grossir ?", "continuer ?")
                if cont == 1:
                    attaque_C = buttonbox("Que veux tu faire ?", "Attaque", ["Griffe", "Morsure"])
                    HP_mob = HP_mob - attaque_L
                    print(HP_mob)
                    affichage = True
                else:
                    mode = "2"
                    choixM2 = False
                    affichage = True
                    XL = 0
                    YL = 100
            else:
                mode = "3"
                PO = PO + 1000
                choixM2 = False
                affichage = True
                disp = True
                XL = 0
                YL = 100

        # Cochon de brique
        elif HP_S == 300:
            if affichage:
                screen.blit(fond_c, (0, 0))
                screen.blit(loup, [100, 200])
                screen.blit(cochon_brique, (XC, YC))
                affichage = False
            elif HP_mob > 0:
                cont = ccbox("Dévorer ce cochon ou le laisser grossir ?", "continuer ?")
                if cont == 1:
                    attaque_C = buttonbox("Que veux tu faire ?", "attaque", ["griffes", "morsure"])
                    HP_mob = HP_mob - attaque_L
                    HP_mob = HP_mob + 50
                    PO = PO + 100
                    print("HP = " + str(HP_mob))
                    print("+100 PO")
                    print(str(PO)+ " PO")
                    if PO > 2000:
                        msgbox("Il faudrait peut être se procurer une arme pour infliger plus de dégats")
                    affichage = True
                else:
                    mode = "3"
                    choixM3 = False
                    affichage = True
                    XL = 0
                    YL = 100
            else:
                mode = "F"
    # FIN
    if mode == "F" :
        screen.blit(ecran_victoire, (0,0))
        screen.blit(loup, (450, 100))
        """Le jeu se fermera si on clique dessus."""
        if event.type == pygame.MOUSEBUTTONDOWN :
            if event.button == 1 :
                launched = False

    # Actualisation de l'image
    pygame.display.flip()